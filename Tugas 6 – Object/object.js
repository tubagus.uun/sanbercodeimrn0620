var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    let i = 1;
    let umur = 'Invalid birth year'

    for (n of arr) {
        let nama = n[0] + " " + n[1]
        if (n[3] != null && n[3] <= thisYear)
            umur = thisYear - n[3]
        else
            umur = 'Invalid birth year'
        let orang = {
            firstName: n[0],
            lastName: n[1],
            gander: n[2],
            age: umur
        }
        console.log(i + ". " + nama + " : ", orang)
        i++
    }
}

console.log("\n\n1. Array to Object")
console.log("===============================================================\n")
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);

/*
 1. Bruce Banner: { 
     firstName: "Bruce",
     lastName: "Banner",
     gender: "male",
     age: 45
 }
 2. Natasha Romanoff: { 
     firstName: "Natasha",
     lastName: "Romanoff",
     gender: "female".
     age: "Invalid Birth Year"
 }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


let barang = function (nama, harga) {
    this.nama = nama;
    this.harga = harga;
}

function shoppingTime(memberId, money) {
    if (memberId == null || memberId == '')
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    if (money < 50000)
        return 'Mohon maaf, uang tidak cukup'

    let brgList = []
    brgList.push(new barang("Sepatu brand Stacattu", 1500000))
    brgList.push(new barang("Baju brand Zoro", 500000))
    brgList.push(new barang("Baju brand H&N", 250000))
    brgList.push(new barang("Sweater brand Uniklooh", 175000))
    brgList.push(new barang("Casing Handphone", 50000))

    let listPurchased = []
    changeMoney = money
    for (let i = 0; i < brgList.length; i++) {
        if (changeMoney < brgList[i].harga)
            continue;
        listPurchased.push(brgList[i].nama)
        changeMoney = changeMoney - brgList[i].harga
    }
    let shopingList = {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: changeMoney
    }
    return shopingList
}


console.log("\n\n2. Shopping Time")
console.log("===============================================================\n")
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let listRutePenumpang = []
    for(arr of arrPenumpang){
        let a = {
            penumpang: arr[0], 
            naikDari: arr[1], 
            tujuan: arr[2], 
        }
        let isrc = rute.indexOf(a.naikDari);
        let idest = rute.indexOf(a.tujuan);
        let panjang = idest - isrc
        a.bayar = panjang*2000
        listRutePenumpang.push(a)
    }
    
    return listRutePenumpang;
}

console.log("\n\n3. Naik Angkot")
console.log("===============================================================\n")
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]