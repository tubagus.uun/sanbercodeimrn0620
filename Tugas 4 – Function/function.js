
console.log("1. Function return String")
console.log("===================================================")
function teriak() {
    return "Halo Sanbers"
}

console.log(teriak())

console.log("\n\n2. Function Kali Input Param Dengan Return ")
console.log("===================================================")
function kalikan(a, b) {
    return a * b;
}

let num1 = 12
let num2 = 4
let hasilKali = kalikan(num1, num2)
console.log(hasilKali)

console.log("\n\n3. Function introduce Input Param Dengan Return ")
console.log("===================================================")
let introduce = function (name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}

let name = "Agus"
let age = 30
let address = "Jln. Malioboro, Yogyakarta"
let hobby = "Gaming"

let perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
