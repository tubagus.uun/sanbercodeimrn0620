/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    constructor(subject, points, email) {
        this.subject = subject;
        this.points = points;
        this.email = email;
    }

    average = () => {
        if (Array.isArray(this.points)) {
            let sum = this.points.reduce((a, b) => a + b);
            let avg = sum / this.points.length
            return avg
        } else {
            return this.points
        }
    }

    output = () => {
        let a = {
            email: this.email,
            subject: this.subject,
            points: this.average()
        }
        return a
    }
}

console.log("\n 1. SOAL CLASS SCORE")
console.log("=================================================")
let sc1 = new Score("quiz", [6, 9], "orang@gmail.com")
console.log("Average : "+sc1.average())
let sc2 = new Score("quiz", 9, "orang2@gmail.com")
console.log("Output : ",sc2.output())

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 
 
  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"
 
  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    let headerbaru = data[0].map((x) => x.replace(" ", "").replace(" ", '') );
    let indek = 0;
    for (let b = 0; b < headerbaru.length; b++) {
        if (headerbaru[b] == subject) {
            indek = b
            break
        }
    }
    let output = []
    for (let i = 1; i < data.length; i++) {
        let sc = new Score(data[0][indek], data[i][indek], data[i][0])
        output.push(sc.output())
    }

    console.log(output);
}

// TEST CASE
console.log("\n 2. SOAL Create Score")
console.log("=================================================")
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"
 
    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate
 
    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate
 
    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant
 
    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour
 
*/

console.log("\n 3. SOAL Recap Score")
console.log("=================================================")
function recapScores(data) {
    for (let i = 1; i < data.length; i++) {
        const [email, ...nilaiarr] = data[i];
        let sc = new Score("quiz", nilaiarr, email)
        let nilai = sc.average().toFixed(1)
        let pred = "";
        if(nilai > 70) pred =  "participant"
        else if(nilai > 80) pred = "graduate"
        else if(nilai > 90) pred = "honour"
        else pred =  "participant"

        console.log(i+". Email : "+ sc.email)
        console.log("Rata-rata : "+ nilai)
        console.log("Predikat : "+ pred+"\n")
    }
}

recapScores(data);