import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Component from './Latihan/Component/Component';
import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import RegisterScreen from './Tugas/Tugas13/RegisterScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';

import Note from './Tugas/Tugas14/App';
import SkillScreen from './Tugas/Tugas14/SkillScreen';

import Navi from './Tugas/Tugas15/index'
import TugasNavi from './Tugas/TugasNavigation/index'

import Quiz3 from './Quiz3/index'

export default function App() {
  return (

    <Quiz3/>

    // <TugasNavi />
    // <Navi/>

    // <SkillScreen />
    // <Note/>

    // <AboutScreen/>
    // <RegisterScreen/>
    // <LoginScreen/>
    
    // <YoutubeUI/>
    // <Component />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
