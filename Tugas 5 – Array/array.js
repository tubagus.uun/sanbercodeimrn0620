
console.log("1. (Range)")
console.log("===================================================")
function range(start, finish) {
    if (start == null || finish == null)
        return -1;
    let arr = [];
    let i = start;
    let asc = start < finish;
    while (true) {
        arr.push(i)
        if (asc) {
            i++;
            if (i > finish)
                break;
        } else {
            i--;
            if (i < finish)
                break;
        }
    }
    return arr
}

console.log(range(5, 1));


console.log("\n\n2. rangeWithStep")
console.log("===================================================")
function rangeWithStep(start, finish, step = 1) {
    if (start == null || finish == null)
        return -1;
    let arr = [];
    let i = start;
    let asc = start < finish;
    while (true) {
        arr.push(i)
        if (asc) {
            i += step;
            if (i > finish)
                break;
        } else {
            i -= step;
            if (i < finish)
                break;
        }
    }
    return arr
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(5, 2, 1));

console.log("\n\n3. Sum of Range")
console.log("===================================================")
function sum(start, finish, step = 1) {
    let jumlah = 0;
    if (start == null && finish == null)
        return jumlah;
    else if (finish == null)
        return start
    else if (start == null)
        return finish;

    let arr = [];
    let i = start;

    let asc = start < finish;
    while (true) {
        arr.push(i)
        jumlah += i;
        if (asc) {
            i += step;
            if (i > finish)
                break;
        } else {
            i -= step;
            if (i < finish)
                break;
        }
    }
    return jumlah
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum(9));
console.log(sum(null, 8));
console.log(sum());

console.log("\n\n4. Array Multidimensi")
console.log("===================================================")
function dataHandling(n) {
    let arrY, arrX;
    for (arrY of n) {
        console.log("\nNomor ID : " + arrY[0])
        console.log("Nama Lengkap : " + arrY[1])
        console.log("TTL : " + arrY[2] + " " + arrY[3])
        console.log("Hobi : " + arrY[4])
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

dataHandling(input)

console.log("\n\n5. Balik Kata")
console.log("===================================================")
function balikKata(kata) {
    let le = kata.length - 1;
    let res = "";
    for (le; le >= 0; le--) {
        res += kata[le]
    }
    return res;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers"))

console.log("\n\n6. Metode Array")
console.log("===================================================")
function dataHandling2(ar) {
    ar.splice(1, 4, ar[1] + " Elsharawy", "Provinsi " + ar[2], ar[3], "Pria", "SMA Internasional Metro")
    console.log(ar)
    let nama = ar[1];
    let tgl = ar[3];
    var artgl = tgl.split("/");
    let bulan = artgl[1]
    var bulans = "";
    bulan = parseInt(bulan);
    switch (bulan) {
        case 1:
            bulans = "Januari"
            break;
        case 2:
            bulans = "Februari"
            break;
        case 3:
            bulans = "Maret"
            break;
        case 4:
            bulans = "April"
            break;
        case 5:
            bulans = "Mei"
            break;
        case 6:
            bulans = "Juni"
            break;
        case 7:
            bulans = "Juli"
            break;
        case 8:
            bulans = "Agustus"
            break;
        case 9:
            bulans = "September"
            break;
        case 10:
            bulans = "Oktober"
            break;
        case 11:
            bulans = "November"
            break;
        case 12:
            bulans = "Desember"
            break;
        default:
            bulans = "bulan error"
    }
    console.log(bulans)
    var artglsort =  Array.from(artgl);
    artglsort.sort(function (v1, v2) { return parseInt(v2) - parseInt(v1) });
    console.log(artglsort)
    let stgl = artgl.join("-")
    console.log(stgl)
    console.log(nama.slice(0, 15))
}

input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input)
