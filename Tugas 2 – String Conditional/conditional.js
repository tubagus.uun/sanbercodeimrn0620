var nama = "Junaedi"
var peran = "Werewolf"

// Output untuk Input nama = '' dan peran = ''
if (nama == '' && peran == '') {

    console.log("Nama harus diisi!");

} else if (nama == 'John' && peran == '') {

    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");

} else if (nama == 'Jane' && peran == 'Penyihir') {

    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");

} else if (nama == 'Jenita' && peran == 'Guard') {

    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf!");

} else if (nama == 'Junaedi' && peran == 'Werewolf') {

    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");

}

console.log("===================================================================================")

var hari = 21;
var bulan = 8;
var tahun = 1945;
var bulans = "";
switch (bulan) {
    case 1:
        bulans = "Januari"
        break;
    case 2:
        bulans = "Februari"
        break;
    case 3:
        bulans = "Maret"
        break;
    case 4:
        bulans = "April"
        break;
    case 5:
        bulans = "Mei"
        break;
    case 6:
        bulans = "Juni"
        break;
    case 7:
        bulans = "Juli"
        break;
    case 8:
        bulans = "Agustus"
        break;
    case 9:
        bulans = "September"
        break;
    case 10:
        bulans = "Oktober"
        break;
    case 11:
        bulans = "November"
        break;
    case 12:
        bulans = "Desember"
        break;
    default:
        bulans = "error"
}

if(hari < 1 || hari > 31 || tahun < 1900 || tahun > 2200){
    bulans = "error"
}
if(bulans!="error"){
    var hasil = hari + " " + bulans + " " + tahun
    console.log(hasil)
}else{
    console.log("Salah Input Hari, Bulan, Tahun")
}

