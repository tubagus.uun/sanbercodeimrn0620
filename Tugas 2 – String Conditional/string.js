var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var hasil = word+' '+second+' '+third+' '+fourth+' '+fifth+' '+sixth+' '+seventh;
console.log(hasil)

console.log("============================================")
var sentence = "I am going to be React Native Developer"; 
var sentences = sentence.split(' ')
var exampleFirstWord = sentences[0] ; 
var secondWord =  sentences[1]; 
var thirdWord = sentences[2];  
var fourthWord = sentences[3];
var fifthWord = sentences[4];
var sixthWord = sentences[5];
var seventhWord = sentences[7];
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 

console.log("============================================")
var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log("============================================")


var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length  
var thirdWordLength = thirdWord3.length  
var fourthWordLength = fourthWord3.length  
var fifthWordLength = fifthWord3.length  

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3+ ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3+ ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3+ ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3+ ', with length: ' + fifthWordLength); 
