let maju = true;
let i = 0, awal = 0, akhir = 20

console.log("LOOPING PERTAMA")
while (maju) {
    i += 2;
    console.log(i + " - I love coding")
    if (i >= akhir)
        maju = false;
}

console.log("LOOPING KEDUA")
while (!maju) {
    console.log(i + " - I will become a mobile developer")
    i -= 2;
    if (i <= awal)
        maju = true;
}

console.log("\nFOR\n=======================================")
let kata;
for (i = 1; i <= 20; i++) {
    if (!(i % 3) && (i % 2)) {
        kata = "I Love Coding"
    } else if (i % 2) {
        kata = "Santai"
    } else {
        kata = 'Berkualitas'
    }
    console.log(i + " - " + kata)
}

console.log("\nPersegi Panjang\n=======================================\n")
kata = "";
for (awal = 0; awal < 4; awal++) {
    for (akhir = 0; akhir < 8; akhir++) {
        kata += "#"
    }
    console.log(kata)
    kata = "";
}


console.log("\nTangga\n=======================================\n")
kata = "";
for (awal = 0; awal < 7; awal++) {
    for (akhir = 0; akhir <= awal; akhir++) {
        kata += "#"
    }
    console.log(kata)
    kata = "";
}

console.log("\nPapan Catur\n=======================================\n")
kata = "";
for (awal = 1; awal <= 8; awal++) {
    for (akhir = 1; akhir <= 8; akhir++) {
        if (awal % 2) {
            if (akhir % 2)
                kata += " "
            else
                kata += "#"
        } else {
            if (akhir % 2)
                kata += "#"
            else
                kata += " "
        }

    }
    console.log(kata)
    kata = "";
}

